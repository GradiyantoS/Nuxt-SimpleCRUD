import axios from '~/plugins/axios'

export const state = () =>({
    users:[]
})

export const mutations= {
    setData(state,items){
        state.users=items
    },

    addData(state,item){
        state.users.push(item)
    },
    
    removeData(state,id){
        const foundIndex = state.users.findIndex(p=> p._id == id)
        state.users.splice(foundIndex,1)
    },

    updateData(state,data){
        const foundItem = state.users.find(p=> p._id == data.id)
        foundItem.name = data.user.name
        foundItem.email = data.user.email
        foundItem.mobile = data.user.mobile
        
    },
}


export const actions ={
    async nuxtServerInit({commit}){
        const res = await axios.get('users')
        commit('setData', res.data)      
    },

    async add({commit},user){
        const res=await axios.post('users',user)
        commit('addData', res.data)      
    },

    async remove({commit},id){
         await axios.delete('users/'+id)
        commit('removeData', id)      
    },

    async get({commit},id){
        const res = await axios.get('users/'+id)
        return res.data
    },

    async update({commit},data){
        const res = await axios.put('users/'+data.id,data.data)
        console.log(data)
        commit('updateData',{id:data.id,user:data.update})
    }

}